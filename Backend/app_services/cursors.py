from flask import Flask, request, jsonify, current_app, send_from_directory
import pymysql
from functools import wraps
from passlib.hash import sha256_crypt
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token
import os
import os.path
from werkzeug import secure_filename
import zipfile
from correo import Correo
from correo_r import Correor


class BaseDatos:

    def __init__(self):
        self.app = Flask(__name__)
        self.bcrypt = Bcrypt(self.app)
        self.jwt = JWTManager(self.app)

        self.app.config['SECRET_KEY' ] = 'thisissecret'
        self.app.config['UPLOAD_FOLDER'] = '/root/registro/registro_hackaton/Backend/app_services/static/images'
        
        # Datos de la base de datos
        self.host = "10.."
        self.user = "user_hackaton"
        self. password = "hpfoa2019"
        self.db = "hackaton"
        self.con = pymysql.connect(host=self.host, port=self.port, user=self.user, password=self.password, db=self.db, cursorclass=pymysql.cursors.DictCursor)

        self.mail = Correo()
        self.mail2 = Correor()

    # Crea un nuevo participante en la bd
    def nuevo_participante(self, request):
        data = request.get_json()
        cur = self.con.cursor()
        
        curp = data['curp']
        result = cur.execute("SELECT id_participante from participantes WHERE curp = %s", [curp])

        if cur.fetchone() != None:
            return jsonify({'Titulo': 'Error en la CURP',
                            'Mensaje': 'Esta CURP ya está registrada',
                            'Tipo': 'warning'})

        correo = data['correo']
        result = cur.execute("SELECT id_participante from participantes WHERE correo = %s", [correo])

        if cur.fetchone() != None:
            return jsonify({'Titulo': 'Error en el correo',
                            'Mensaje': 'Este Correo ya está registrado',
                            'Tipo': 'warning'})
        
        nombre = data['nombre']
        apellido_paterno = data['apellido_paterno']
        apellido_materno = data['apellido_materno']
        identificacion_oficial = data['identificacion_oficial']
        foto_identificacion_adelante = ''
        foto_identificacion_atras = ''
        sexo = data['sexo']
        fecha_nacimiento = data['fecha_nacimiento']
        numero_telefono = data['numero_telefono']
        edad = data['edad']
        contrasena = data['contrasena']
        estado = data['estado']
        municipio = data['municipio']
        localidad = data['localidad']
        codigo_postal = data['codigo_postal']
        tipo_vialidad = data['tipo_vialidad']
        nombre_vialidad = data['nombre_vialidad']
        numero_exterior = data['numero_exterior']

        if not 'numero_interior' in data or data['numero_interior'] == '':
            numero_interior = None
        else:
            numero_interior = data['numero_interior']

        asentamiento = data['asentamiento']
        entre_vialidad_1 = data['entre_vialidad_1']
        entre_vialidad_2 = data['entre_vialidad_2']
        descripcion_ubicacion = data['descripcion_ubicacion']
        etapa_proyecto = data['etapa_proyecto']
        area_proyecto = data['area_proyecto']
        estado_civil = data['estado_civil']
        jefe_familia = data['jefe_familia']
        ocupacion = data['ocupacion']
        ingreso_mensual = data['ingreso_mensual']
        numero_integrantes_familia = data['numero_integrantes_familia']
        numero_dependientes = data['numero_dependientes']
        nivel_estudios = data['nivel_estudios']
        escuela = data['escuela']
        tipo_seguridad_social = data['tipo_seguridad_social']
        discapacidad = data['discapacidad']
        grupos_vulnerables = data['grupos_vulnerables']
        como_enteraste_evento = data['como_enteraste_evento']
        tipo_vivienda = data['tipo_vivienda']
        electricidad = data['electricidad']
        agua = data['agua']
        drenaje = data['drenaje']
        telefono = data['telefono']
        internet = data['internet']
        revisado = 'no'
        activo = 1

        # Encriptamos la password
        password = sha256_crypt.encrypt(str(contrasena))

        cur.execute("INSERT INTO participantes(curp, nombre, apellido_paterno, apellido_materno, "
        "identificacion_oficial, foto_identificacion_adelante, foto_identificacion_atras, "
        "sexo, fecha_nacimiento, numero_telefono, edad, correo, contrasena, estado, municipio, localidad, "
        "codigo_postal, tipo_vialidad, nombre_vialidad, numero_exterior, numero_interior, asentamiento, "
        "entre_vialidad_1, entre_vialidad_2, descripcion_ubicacion, etapa_proyecto, area_proyecto, "
        "estado_civil, jefe_familia, ocupacion, ingreso_mensual, numero_integrantes_familia, "
        "numero_dependientes, nivel_estudios, escuela, tipo_seguridad_social, discapacidad, "
        "grupos_vulnerables, como_enteraste_evento, tipo_vivienda, electricidad, "
        "agua, drenaje, telefono, internet, revisado, activo) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        "%s, %s, %s, %s, %s, %s)", (curp, nombre, apellido_paterno, apellido_materno,
        identificacion_oficial, foto_identificacion_adelante, foto_identificacion_atras,
        sexo, fecha_nacimiento, numero_telefono, edad, correo, password, estado, municipio, localidad,
        codigo_postal, tipo_vialidad, nombre_vialidad, numero_exterior, numero_interior, asentamiento,
        entre_vialidad_1, entre_vialidad_2, descripcion_ubicacion, etapa_proyecto, area_proyecto,
        estado_civil, jefe_familia, ocupacion, ingreso_mensual, numero_integrantes_familia,
        numero_dependientes, nivel_estudios, escuela, tipo_seguridad_social, discapacidad,
        grupos_vulnerables, como_enteraste_evento, tipo_vivienda, electricidad,
        agua, drenaje, telefono, internet, revisado, activo))
        ##Commit to DB
        self.con.commit()

        # Actualizar los participantes registrados
        result = cur.execute("SELECT num_registros FROM registros WHERE id_registro = 1")
        registros = cur.fetchone()
        num_registros = registros['num_registros']
        num_registros += 1
        cur.execute("UPDATE registros SET num_registros = %s WHERE id_registro = 1;", [num_registros])
        self.con.commit()

        # Sacar el id participante
        result = cur.execute("SELECT id_participante from participantes WHERE curp = %s", [curp])
        participante = cur.fetchone()
        id_participante = participante['id_participante']

        # Enviamos correo
        self.mail.enviar_correo(correo, nombre, apellido_paterno, apellido_materno, id_participante)

        # Cerramos la conexión
        cur.close()

        return jsonify({'Titulo': 'Registrado con éxito',
                        'Mensaje': 'Registro correcto, después recibiras un correo con tu ID de emprendedor',
                        'Tipo': 'success',
                        'ID': id_participante})

    # Obtener todos los participantes de la bd
    def obtener_participantes(self):
        try:
            cur = self.con.cursor()

            result = cur.execute("SELECT id_participante, curp, nombre, apellido_paterno, apellido_materno, "
            "identificacion_oficial, foto_identificacion_adelante, foto_identificacion_atras, "
            "sexo, DATE_FORMAT(fecha_nacimiento, '%d %M %Y') AS 'fecha_nacimiento', numero_telefono, edad, correo, contrasena, estado, municipio, localidad, "
            "codigo_postal, tipo_vialidad, nombre_vialidad, numero_exterior, numero_interior, asentamiento, "
            "entre_vialidad_1, entre_vialidad_2, descripcion_ubicacion, etapa_proyecto, area_proyecto, "
            "estado_civil, jefe_familia, ocupacion, ingreso_mensual, numero_integrantes_familia, "
            "numero_dependientes, nivel_estudios, escuela, tipo_seguridad_social, discapacidad, "
            "grupos_vulnerables, como_enteraste_evento, tipo_vivienda, electricidad, "
            "agua, drenaje, telefono, internet, revisado FROM participantes WHERE activo = 1")
            data = cur.fetchall()
            cur.close()
            return data
        except:
            return 'Ups! Hemos tenido problemas'

    def revisar_participante(self, id_participante):
        try:
            cur = self.con.cursor()
            revisado = 'si'
            cur.execute("UPDATE participantes SET revisado = %s WHERE id_participante = %s", [revisado, id_participante])
            self.con.commit()
            cur.execute("SELECT * from participantes WHERE id_participante = %s", [id_participante])
            participante = cur.fetchone()
            correo = participante['correo']
            nombre = participante['nombre']
            self.mail2.enviar_correo2(correo, nombre, id_participante)
            cur.close()
            return 'Revisado con éxito'
        except:
            return 'Ups! Hemos tenido problemas'

    def obtener_participante(self, id_participante):
        try:
            cur = self.con.cursor()
            result = cur.execute("SELECT id_participante, curp, nombre, apellido_paterno, apellido_materno, "
            "identificacion_oficial, foto_identificacion_adelante, foto_identificacion_atras, "
            "sexo, DATE_FORMAT(fecha_nacimiento, '%d %M %Y') AS 'fecha_nacimiento', numero_telefono, edad, correo, contrasena, estado, municipio, localidad, "
            "codigo_postal, tipo_vialidad, nombre_vialidad, numero_exterior, numero_interior, asentamiento, "
            "entre_vialidad_1, entre_vialidad_2, descripcion_ubicacion, etapa_proyecto, area_proyecto, "
            "estado_civil, jefe_familia, ocupacion, ingreso_mensual, numero_integrantes_familia, "
            "numero_dependientes, nivel_estudios, escuela, tipo_seguridad_social, discapacidad, "
            "grupos_vulnerables, como_enteraste_evento, tipo_vivienda, electricidad, "
            "agua, drenaje, telefono, internet, revisado FROM participantes WHERE id_participante = %s", [id_participante])
            data = cur.fetchone()
            cur.close()
            return data
        except:
            return 'Ups! Hemos tenido problemas'

    # Eliminar un particioante de la bd
    def eliminar_participante(self, id_participante):
        cur = self.con.cursor()
        cur.execute("SELECT * FROM participantes WHERE id_participante = %s", ([id_participante]))
        participante = cur.fetchone()
        
        correo = participante['correo']
        nombre = participante['nombre']
        apellido_paterno = participante['apellido_paterno']
        apellido_materno = participante['apellido_materno']

        self.mail.correo_rechazo(correo, nombre, apellido_paterno, apellido_materno)

        # Sacar las rutas de las imagenes
        ruta_adelante = participante['foto_identificacion_adelante']
        ruta_atras = participante['foto_identificacion_atras']

        lon_adelante = len(ruta_adelante)
        lon_atras = len(ruta_atras)

        ruta_adelante = ruta_adelante[1:lon_adelante]
        ruta_atras = ruta_atras[1:lon_atras]

        # Eliminar el oarticipante de la base de datos
        cur.execute("SELECT * FROM participantes")

        if cur.fetchone() != None:
            cur.execute("UPDATE participantes SET activo = 0 WHERE id_participante = %s", [id_participante])
            #Eliminar las imagenes
            # os.remove(ruta_adelante)
            # os.remove(ruta_atras)

            ##Commit to DB
            self.con.commit()
            cur.close()
            
            return "Participante eliminado correctamente!"
        return "Este participante no existe, intente de nuevo."
        cur.close()

    # Obtener los estados de la bd
    def obtener_estados(self):
        try:
            cur = self.con.cursor()
            result = cur.execute("SELECT nombre FROM estados")
            data = cur.fetchall()
            cur.close()
            return data
        except:
             return 'Ups! Hemos tenido problemas'

    # Obtener los municipos de la bd
    def obtener_municipios(self, nombre_entidad):
        try:
            cur = self.con.cursor()
            result = cur.execute("SELECT m.nombre FROM municipios m JOIN estados e ON m.estado_id = e.id WHERE e.nombre = %s", [nombre_entidad])
            data = cur.fetchall()
            cur.close()
            return data
        except:
             return 'Ups! Hemos tenido problemas'

    def registrar_administrador(self, request):
        data = request.get_json()

        if not data or not 'usuario' in data or not 'password' in data:
            return jsonify({'Mensaje': 'Faltan datos'})

        usuario = data['usuario']
        password = self.bcrypt.generate_password_hash(data['password']).decode('utf-8')

        cur = self.con.cursor()
        cur.execute("INSERT INTO administradores(usuario, password) VALUES(%s, %s)", ([usuario, password]))
        self.con.commit()
        cur.close()

        result = {
            'usuario': usuario,
            'password': password,
        }

        return jsonify({'Administrador': result})

    def login(self, request):
        data = request.get_json()
        usuario = data['usuario']
        password = data['password']

        cur = self.con.cursor()
        cur.execute("SELECT * FROM administradores WHERE usuario = %s", [usuario])
        usuario = cur.fetchone()

        if usuario != None:
            if self.bcrypt.check_password_hash(usuario['password'], password):
                access_token = create_access_token(identity = {'usuario': usuario['usuario']})
                return jsonify({'token': access_token})
            else:
                return jsonify({'Titulo': 'Error en la contraseña',
                                'Mensaje': 'La contraseña es incorrecta',
                                'Tipo': 'warning'})

        else:
            return jsonify({'Titulo': 'Error en el usuario',
                                'Mensaje': 'El usuario no existe',
                                'Tipo': 'warning'})

    def subir_frontal(self, id_participante, request):
        if request.method == 'POST':
            f = request.files['archivo']
            filename = secure_filename(f.filename)
            extension = os.path.splitext(filename)[1]

            cur = self.con.cursor()
            
            cur.execute("SELECT curp FROM participantes WHERE id_participante = %s", [id_participante])
            curp = cur.fetchone()
            curp1 = curp['curp']

            # Guardamos el archivo en el directorio "imagenes"
            f.save(os.path.join(self.app.config['UPLOAD_FOLDER'], filename))

            nombre = '/root/registro/registro_hackaton/Backend/app_services/static/images/' + filename
            nombre_nuevo = '/root/registro/registro_hackaton/Backend/app_services/static/images/' + str(id_participante) + '_' + str(curp1) + '_frontal' + extension

            cur.execute("UPDATE participantes SET foto_identificacion_adelante = %s WHERE id_participante = %s", [nombre_nuevo, id_participante])
            self.con.commit()
            cur.close()
            os.rename(nombre, nombre_nuevo)

            return jsonify({'Imagen': nombre_nuevo})

    def subir_atras(self, id_participante, request):
        if request.method == 'POST':
            # obtenemos el archivo del input "archivo"
            f = request.files['archivo']
            filename = secure_filename(f.filename)
            extension = os.path.splitext(filename)[1]

            cur = self.con.cursor()
            
            cur.execute("SELECT curp FROM participantes WHERE id_participante = %s", [id_participante])
            curp = cur.fetchone()
            curp1 = curp['curp']
            
            # Guardamos el archivo en el directorio "imagenes"
            f.save(os.path.join(self.app.config['UPLOAD_FOLDER'], filename))

            nombre = '/root/registro/registro_hackaton/Backend/app_services/static/images/' + filename
            nombre_nuevo = '/root/registro/registro_hackaton/Backend/app_services/static/images/' + str(id_participante) + '_' + str(curp1) + '_atras' + extension
            cur.execute("UPDATE participantes SET foto_identificacion_atras = %s WHERE id_participante = %s", [nombre_nuevo, id_participante])
            self.con.commit()
            cur.close()
            os.rename(nombre, nombre_nuevo)

            return jsonify({'Imagen': nombre_nuevo})

    def downloadFile(self, id_participante):
        cur = self.con.cursor()
        cur.execute("SELECT * FROM participantes WHERE id_participante = %s", ([id_participante]))
        participante = cur.fetchone()
        curp = participante['curp']
        path = '.' + participante['foto_identificacion_adelante']
        path2 = '.' + participante['foto_identificacion_atras']

        uploads = os.path.join(current_app.root_path, self.app.config['UPLOAD_FOLDER'])

        # Crear zip
        nombreZip = "/root/registro/registro_hackaton/Backend/app_services/static/images/" + curp + ".zip"
        jungle_zip = zipfile.ZipFile(nombreZip, 'w')
        jungle_zip.write(path, compress_type=zipfile.ZIP_DEFLATED)
        jungle_zip.write(path2, compress_type=zipfile.ZIP_DEFLATED)
    
        jungle_zip.close()

        filename = curp + ".zip"

        return send_from_directory(directory=uploads, filename=filename, as_attachment=True)

    # En run_this.py agregar "db.token_required" cuando para correr el servicio
    # es necesario iniciar sesion, este metodo comprueba que exista el token "x-access-token"
    # Se descifra y comprueba que exista un usuario para ese token en la bd
    def token_required(self, f):
        @wraps(f)
        def decorated(*args, **kwargs):
            token = None
            if 'x-access-token' in request.headers:
                token = request.headers['x-access-token']

            if not token:
                return jsonify({'message' : 'Iniciar sesión primero'}), 401

            try:
                data = jwt.decode(token, self.app.config['SECRET_KEY'])
                cur = self.mysql.connection.cursor()
                result = cur.execute("SELECT usuario FROM administradores WHERE usuario=%s", [data['usuario']])
                if result > 0:
                    data = cur.fetchone()
                    current_user = data['id_administrador']
                ##Cerrar conexion
                else:
                    return jsonify({'message' : 'Token is invalid!'}), 401
                cur.close()

            except:
                return jsonify({'message' : 'Token is invalid!'}), 401
            return f(current_user, *args, **kwargs)
        return decorated
