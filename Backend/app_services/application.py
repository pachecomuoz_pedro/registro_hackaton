from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from cursors import BaseDatos

# Inicializar
app = Flask(__name__)
bcrypt = Bcrypt(app)
db = BaseDatos()
CORS(app)
jwt = JWTManager(app)

app.config['SECRET_KEY' ] = 'thisissecret'
serving.run_simple(API_HOST, API_PORT, app, ssl_context=context)
'/etc/pki/tls/certs/ssl_certificate_godezac.crt', '/etc/pki/tls/private/godezac.key'
/etc/pki/ca-trust/extracted/pem/Intermediate_godezac.crt
# Método para registro
@app.route('/agregar_participante', methods=['POST'])
def register():
    return db.nuevo_participante(request)

@app.route('/obtener_participantes', methods=['GET'])
def obtener_participantes():
    return jsonify(db.obtener_participantes())

@app.route('/revisar_participante/<int:id_participante>', methods=['PUT'])
def revisar_participante(id_participante):
    return jsonify({'Mensaje': db.revisar_participante(id_participante)})

@app.route('/obtener_participante/<int:id_participante>', methods=['GET'])
def obtener_participante(id_participante):
    return jsonify(db.obtener_participante(id_participante))

@app.route('/obtener_participantes_guardar', methods=['GET'])
def obtener_participantes_guardar():
    return jsonify({'Participantes': db.obtener_participantes()})
    
@app.route('/eliminar_participante/<int:id_participante>', methods=['PUT'])
def eliminar_participante(id_participante):
    return jsonify({"message" : db.eliminar_participante(id_participante)})

@app.route('/obtener_estados', methods=['GET'])
def obtener_estados():
    return jsonify({"message" : db.obtener_estados()})

@app.route('/obtener/municipios/<string:nombre_entidad>', methods=['GET'])
def obtener_municipios(nombre_entidad):
    return jsonify({"message" : db.obtener_municipios(nombre_entidad)})

@app.route('/registrar_administrador', methods=['POST'])
def registrar_administrador():
    return db.registrar_administrador(request)

@app.route('/login', methods=['POST'])
def login():
    return db.login(request)

@app.route("/subir_frontal/<int:id_participante>", methods=['POST'])
def subir_frontal(id_participante):
    return db.subir_frontal(id_participante, request)

@app.route("/subir_atras/<int:id_participante>", methods=['POST'])
def subir_atras(id_participante):
    return db.subir_atras(id_participante, request)

@app.route('/download/<int:id_participante>', methods=['GET', 'POST'])
def downloadFile(id_participante):
    return db.downloadFile(id_participante)

if __name__ == '__main__':
    app.run(debug=True)
