from flask import Flask, render_template
from flask_mail import Mail, Message


class Correo:

    def __init__(self):
        self.app = Flask(__name__)

        # Configuracion de gmail
        self.app.config['MAIL_SERVER'] = 'smtp.googlemail.com'
        self.app.config['MAIL_PORT'] = 465
        self.app.config['MAIL_USE_TLS'] = False
        self.app.config['MAIL_USE_SSL'] = True
        self.app.config['MAIL_USERNAME'] = 'pregistrosezac@gmail.com'
        self.app.config['MAIL_PASSWORD'] = 'hpfoa2019'

        self.mail = Mail(self.app)
    
    def enviar_correo(self, user_email, nombre, apellido_paterno, apellido_materno, id_participante):
        msg = Message('Gracias por tu registro!', sender = self.app.config['MAIL_USERNAME'], recipients = [user_email])
        msg.html = render_template('email.html', nombre = nombre, id_participante = id_participante, apellido_paterno = apellido_paterno, apellido_materno = apellido_materno)
        with self.app.app_context():
            self.mail.send(msg)

    def correo_rechazo(self, user_email, nombre, apellido_paterno, apellido_materno):
        msg = Message('Hay problemas con tus datos!', sender = self.app.config['MAIL_USERNAME'], recipients = [user_email])
        msg.html = render_template('email_rechazado.html', nombre = nombre, apellido_paterno = apellido_paterno, apellido_materno = apellido_materno)
        with self.app.app_context():
            self.mail.send(msg)
