/* Rutas */
import { RouterModule, Routes } from '@angular/router';

/* Componentes */
import { AuthGuardService } from './auth-guard.service';
import { RegistroComponent } from './pages/registro/registro.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { FormularioComponent } from './pages/formulario/formulario.component';
import { LoginAdminComponent } from './pages/login-admin/login-admin.component';
import { PanelAdminComponent } from './pages/panel-admin/panel-admin.component';




/* Establecemos las rutas de nuestra aplicación */
const appRoutes: Routes = [
    { path: '', component: RegistroComponent },
    { path: 'formulario', component: FormularioComponent},
    { path: 'login', component: LoginAdminComponent},
    { path: 'administrador', component: PanelAdminComponent, canActivate: [AuthGuardService]}
    //{ path: '', redirectTo: '/registro', pathMatch: 'full' }
    //{ path: '**', component: NopagefoundComponent }
];


export const APP_ROUTES = RouterModule.forRoot(appRoutes, {useHash: true} );
