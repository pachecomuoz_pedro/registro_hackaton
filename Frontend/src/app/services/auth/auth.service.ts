import { Injectable } from '@angular/core';
import { Administrador } from '../../modelos/Administrador';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor() { }

  public login(usuarioInfo: Administrador) {
    localStorage.setItem('ACCESS_TOKEN', 'access_token');

  }

  public isLoggedIn() {
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  public logout() {
    localStorage.removeItem('ACCESS_TOKEN');
  }
}

