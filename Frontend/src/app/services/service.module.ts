import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
        SharedService
      } from './service.index';
import { FormularioService } from './formulario/formulario.service';
import {AuthGuardService} from './authGuard/auth-guard.service';
import { AuthService } from './auth/auth.service';


@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    SharedService,
    FormularioService,
    AuthGuardService,
    AuthService
  ],
  declarations: []

})
export class ServiceModule { }
