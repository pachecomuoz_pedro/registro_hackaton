import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import { URL_SERVICIOS } from '../../../config/urls'
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class FormularioService {

  constructor(private http:  HttpClient) { }

  agregarParticipante(data: Object): Observable<any>{
    return this.http.post(URL_SERVICIOS + '/agregar_participante', data, httpOptions)
  }
}
