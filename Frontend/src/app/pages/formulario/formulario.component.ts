import { Component, OnInit } from '@angular/core';
import { FormularioService } from '../../services/service.index';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { URL_SERVICIOS } from '../../../config/urls';
import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
// import { NgxCaptchaModule } from 'ngx-captcha';
import { NgModule } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss'],
  providers: [FormularioService]
})

@NgModule({
  imports: [
    ReactiveFormsModule
  ]
})

export class FormularioComponent implements OnInit {
  formulario: FormGroup;
  estados = this.obtenerEstados();
  submitted = false;
  municipios: any;
  participante: any;
  valor: any;
  foto1: any;
  curp: any;
  archivo1: File = null;
  archivo2: File = null;
  archivo3: File = null;
  protected aFormGroup: FormGroup;
  public loading: boolean;
  public bloquearFoto: boolean;
  public restriccionImagen1: boolean;
  public restriccionImagen2: boolean;

  titulo: any;
  mensaje: any;
  tipo: any;

  constructor(private formularioService: FormularioService, public http: HttpClient, public router: Router,
    private formBuilder: FormBuilder) {
    this.loading = false;
    this.restriccionImagen1 = false;
    this.restriccionImagen2 = false;
    this.bloquearFoto = false;
  }

  ngOnInit() {
    this.formulario = new FormGroup({
      'curp': new FormControl('', [Validators.required, Validators.minLength(18), Validators.maxLength(18), Validators.nullValidator]),
      'confirmar_curp': new FormControl('', [Validators.required]),
      'nombre': new FormControl('', [Validators.required, Validators.maxLength(40)]),
      'apellido_paterno': new FormControl('', Validators.required),
      'apellido_materno': new FormControl('', Validators.required),
      'identificacion_oficial': new FormControl('', Validators.required),
      'foto_identificacion_adelante': new FormControl('', Validators.required),
      'foto_identificacion_atras': new FormControl('', Validators.required),
      'sexo': new FormControl('', Validators.required),
      'fecha_nacimiento': new FormControl('', Validators.required),
      'numero_telefono': new FormControl('', [Validators.required, Validators.minLength(10)]),
      'edad': new FormControl('', Validators.required),
      'correo': new FormControl('', [Validators.required, Validators.email]),
      'contrasena': new FormControl('', [Validators.required]),
      'confirmar_contrasena': new FormControl(''),
      'estado': new FormControl('', Validators.required),
      'municipio': new FormControl('', Validators.required),
      'localidad': new FormControl('', Validators.required),
      'codigo_postal': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'tipo_vialidad': new FormControl('', Validators.required),
      'nombre_vialidad': new FormControl('', Validators.required),
      'numero_exterior': new FormControl('', Validators.required),
      'numero_interior': new FormControl(''),
      'asentamiento': new FormControl('', Validators.required),
      'entre_vialidad_1': new FormControl('', Validators.required),
      'entre_vialidad_2': new FormControl('', Validators.required),
      'descripcion_ubicacion': new FormControl('', Validators.required),
      'etapa_proyecto': new FormControl('', Validators.required),
      'area_proyecto': new FormControl('', Validators.required),
      'estado_civil': new FormControl('', Validators.required),
      'jefe_familia': new FormControl('', Validators.required),
      'ocupacion': new FormControl('', Validators.required),
      'ingreso_mensual': new FormControl('', Validators.required),
      'numero_integrantes_familia': new FormControl('', Validators.required),
      'numero_dependientes': new FormControl('', Validators.required),
      'nivel_estudios': new FormControl('', Validators.required),
      'escuela': new FormControl('', Validators.required),
      'tipo_seguridad_social': new FormControl('', Validators.required),
      'discapacidad': new FormControl('', Validators.required),
      'grupos_vulnerables': new FormControl('', Validators.required),
      'como_enteraste_evento': new FormControl('', Validators.required),
      'tipo_vivienda': new FormControl('', Validators.required),
      'numero_habitantes_vivienda': new FormControl('', Validators.required),
      'electricidad': new FormControl('', Validators.required),
      'agua': new FormControl('', Validators.required),
      'drenaje': new FormControl('', Validators.required),
      'telefono': new FormControl('', Validators.required),
      'internet': new FormControl('', Validators.required),
      'condiciones': new FormControl('', Validators.required)
    });

    this.formulario.controls['confirmar_contrasena'].setValidators([
      Validators.required, this.validarMatchContrasenas.bind(this.formulario)
    ]);

    this.formulario.controls['confirmar_curp'].setValidators([
      Validators.required, this.validarMatchCurp.bind(this.formulario)
    ]);
    this.foto1 = this.formulario.controls['foto_identificacion_adelante'];
    this.curp = this.formulario.controls['curp'];
  }

  validarMatchContrasenas(control: FormControl): any {
    const forma: any = this;

    if (control.value !== forma.controls['contrasena'].value) {
      return {
        noIguales: true
      };
    }
    return null;
  }

  validarMatchCurp(control: FormControl): any {
    const forma: any = this;

    if (control.value !== forma.controls['curp'].value) {
      return {
        noIguales: true
      };
    }
    return null;
  }

  obtenerEstados() {
    this.http.get(URL_SERVICIOS + '/obtener_estados'
    ).subscribe(data => {
      var arreglo = data['message'];
      this.estados = arreglo;
    }, error => {
      console.log(error);
      console.log(error.status);
    });
  }

  obtenerUnMunicipio(value) {
    this.http.get(URL_SERVICIOS + '/obtener/municipios/' + value
    ).subscribe(data => {
      var arreglo = data['message'];
      this.municipios = arreglo;
    }, error => {
      console.log(error);
      console.log(error.status);
    });
  }

  bloquearFotoAtras(value){
    if(value === "Pasaporte" || value === "Cartilla Militar" || value === "Identificación Escolar"){
      this.bloquearFoto = true;
    }else{
      this.bloquearFoto = false;
    }
  }

  onFileSelected1(event) {
    var size = event.target.files[0].size;

    if(Math.round(size / 1024) > 512){
      this.restriccionImagen1 = true;
    }
    else{
      if (event.target.files.length > 0) {
        this.archivo1 = <File>event.target.files[0];
        this.restriccionImagen1 = false;
      }
    }
  }

  onFileSelected2(event) {
    var size = event.target.files[0].size;

    if(Math.round(size / 1024) > 512){
      this.restriccionImagen2 = true;
    }
    else{
      if (event.target.files.length > 0) {
        this.archivo2 = <File>event.target.files[0];
        this.restriccionImagen2 = false;
      }
    }
  }

  subirImagen1(id: number) {
    const url = `/subir_frontal/${id}`
    const fd = new FormData();
    fd.append('archivo', this.archivo1, this.archivo1.name);
    this.loading = true;
    this.http.post(URL_SERVICIOS + url, fd).
      subscribe(res => {});
  }

  subirImagen2(id: number) {
    const url = `/subir_atras/${id}`
    const fd = new FormData();
    this.loading = true;
    fd.append('archivo', this.archivo2, this.archivo2.name);
    this.http.post(URL_SERVICIOS + url, fd).
      subscribe(res => {});
  }

  habilitaBoton(control: FormControl): any {
    control.value
  }
  
  registrarEmprendedor(){
    if (this.formulario.valid) {
      if(this.restriccionImagen1 === false && this.restriccionImagen2 === false){
        this.loading = true;

        this.formularioService.agregarParticipante(JSON.stringify(this.formulario['value'])).subscribe(data => {
          if(data['Tipo'] === 'success'){
            Swal.fire(
              data['Titulo'],
              data['Mensaje'],
              data['Tipo']
            );
            
            this.subirImagen1(data['ID']);

            if(!this.bloquearFoto){
              this.subirImagen2(data['ID']);
            }
            this.loading = false;
            this.router.navigateByUrl('/');
          }else{
            Swal.fire(
              data['Titulo'],
              data['Mensaje'],
              data['Tipo']
            );
            this.loading = false;
          }

        }, err => {
          this.loading = false;
          console.error(err);
        });
      }else{
        this.loading = false;
        Swal.fire(
          'Error',
          'Por favor seleccione imagenes menores a 512 KB',
          'warning'
        );
      }
      
    } else {
      this.loading = false;
      Swal.fire(
        'Error',
        'Faltan algunos datos por ingresar',
        'warning'
      );
    }
  }

  seguroDeRegresar() {
    Swal.fire({
      title: "¿Estás seguro de regresar?",
      text: "Si regresas a la página principal perderas tus datos",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si",
      cancelButtonText: "No"
    }).then((result) => {
      if (result.value) {
        this.router.navigateByUrl('/')
      }
    })
  }
}
