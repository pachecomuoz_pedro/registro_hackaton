var app = angular.module("myApp",[]);

var jsonCombo =[
    {"llave": "Aguascalientes", valor: ["aldame","juan"]},
    {"llave": "Zacatecas", valor: ["Hola","juan"]},
];

app.controller("ctrlCombobox",["$scope",
    function(o){
        o.combobox = jsonCombo;
        o.vlCmb1 = "aldame";

        o.encontrarListaDeCombo = function (idLlaveCombo){
            var max = jsonCombo.length;
            for(var i =0; 1<max; i++){
                var iteracion = o.combobox[i];
                if (idLlaveCombo === iteracion.llave){
                   return iteracion.valor;
                 }
            }
            return [];
        };
        o.cambiarCombo1 = function (idLlaveCombo){
            o.vlCmb2 = o.encontrarListaDeCombo(idLlaveCombo)[0];
        };
        o.cambiarCombo1(o.vlCmb1)
    }

])