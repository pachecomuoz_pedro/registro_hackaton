import {Component} from '@angular/core'
import {AuthenticationService, TokenPayload} from '../../authentication.service'
import {Router} from '@angular/router'
import {FormControl, FormGroup, Validators} from '@angular/forms'
import Swal from 'sweetalert2'

@Component({
    templateUrl: './login-admin.component.html',
    styleUrls: ['./login-admin.component.scss']
})
export class LoginAdminComponent{
    credentials: TokenPayload = {
        id: 0,
        usuario: '',
        password: ''
    }

    adminForm = new FormGroup({
        usuario: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required])
    });

    constructor(private auth: AuthenticationService, private router: Router){}

    login(){
        if(this.adminForm.valid){
            this.auth.login(this.credentials).subscribe(
                data => {
                    if(data['Tipo'] === 'warning'){
                        Swal.fire(
                            data['Titulo'],
                            data['Mensaje'],
                            data['Tipo']
                        )
                    }
                    this.router.navigateByUrl('/administrador')
                },
                err => {
                    console.error(err)
                }
            )
        }else{
            Swal.fire(
                'Error datos faltantes',
                'Introduzca todos los datos',
                'warning'
            )
        }
    }
}