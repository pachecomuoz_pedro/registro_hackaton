import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Participante} from '../../modelos/Participante'
import {URL_SERVICIOS} from '../../../config/urls'

@Injectable()
export class AdministradorService{
    constructor(private http: HttpClient){}

    getParticipantes(): Observable<Participante[]>{
        return this.http.get<Participante[]>(URL_SERVICIOS + '/obtener_participantes')
    }

    revisarParticipante(participante: Participante): Observable<{}>{
        const url = `/revisar_participante/${participante.id_participante}`
        return this.http.put<Participante>(URL_SERVICIOS + url, participante)
    }

    eliminarParticipante(participante: Participante): Observable<{}>{
        const url = `/eliminar_participante/${participante.id_participante}`
        return this.http.put<Participante>(URL_SERVICIOS + url, participante)
    }

    descargarArchivos(participante: Participante){
        const url = `/download/${participante.id_participante}`
        
        return this.http.get(URL_SERVICIOS + url, {responseType: 'arraybuffer'})
    }
}