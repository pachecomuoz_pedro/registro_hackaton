import {HttpClient} from '@angular/common/http'
import {Component, OnInit} from '@angular/core'
import {FormularioService} from '../../services/service.index'
import {Router} from '@angular/router'
import {ExcelService} from '../recursos/excel.service'
import {URL_SERVICIOS} from '../../../config/urls'
import {AuthenticationService} from '../../authentication.service'
import {AdministradorService} from './administrador.service'
import Swal from 'sweetalert2'
import {Participante} from '../../modelos/Participante'

@Component({
  selector: 'app-panel-admin',
  templateUrl: './panel-admin.component.html',
  styleUrls: ['./panel-admin.component.scss'],
  providers: [AdministradorService]
})

export class PanelAdminComponent implements OnInit{
    emprendedores: Participante[]
    revisaParticipante: Participante
    userFilter: any = {nombre: ""}

    constructor(public router: Router, public _formularioService: FormularioService, private administradorService: AdministradorService,
        private http: HttpClient, private excelService: ExcelService, private auth: AuthenticationService) {}

    ngOnInit(){
      this.obtenerParticipantes()
    }

    obtenerParticipantes(): void{
        this.administradorService.getParticipantes().subscribe(emprendedores => (this.emprendedores = emprendedores));
    }

    revisarParticipante(participante){
      Swal.fire({
        title: "Revisar Participante",
        text: "¿Este participante tiene todos sus datos correctos?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
        cancelButtonText: "No"
      }).then((result) => {
        if(result.value){
            this.administradorService.revisarParticipante(participante).subscribe(() => {
              this.obtenerParticipantes()
            })
          Swal.fire(
            'Revisado con éxito',
            'El participante ha sido revisado',
            'success'
          )
        }
      })
    }

    eliminarParticipante(participante): void{
      Swal.fire({
        title: "Rechazar Participante",
        text: "¿Estás seguro de rechazar el participante?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
        cancelButtonText: "No"
      }).then((result) => {
        if(result.value){
          this.administradorService.eliminarParticipante(participante)
          .subscribe(() => {
            this.obtenerParticipantes()
          })

          Swal.fire(
            'Rechazado con éxito',
            'El participante ha sido rechazado de la base de datos',
            'success'
          )
        }
      })
    }

    exportAsXLSX() {
        this.http.get(URL_SERVICIOS + '/obtener_participantes_guardar').subscribe(data => {
        var arreglo = data['Participantes'];
        this.excelService.exportAsExcelFile(arreglo, 'Registro');
        },
        error => {
            console.log(error);
        });
    }

    descargarArchivos(emprendedor: Participante){
      this.administradorService.descargarArchivos(emprendedor).subscribe(data => {
        const blob = new Blob([data], {
          type: 'application/zip',
        });
        const url = window.URL.createObjectURL(blob);
        window.open(url);
      },
        error => {
          console.log(error)
    })
  }

    salir(){
      Swal.fire({
        title: "Cerrar sesión",
        text: "¿Estás seguro de salir de la sesión?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
        cancelButtonText: "No"
      }).then((result) => {
        if(result.value){
          this.auth.logout()
        }
      })
    }
}