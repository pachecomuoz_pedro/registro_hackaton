import {Component} from '@angular/core'
import {AuthenticationService} from '../../authentication.service'

@Component({
    templateUrl: './registro.component.html',
    styleUrls: ['./registro.component.scss']
})

export class RegistroComponent{
    constructor(private auth: AuthenticationService){}
}