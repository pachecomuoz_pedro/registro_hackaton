/* Modulos */
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ArchwizardModule } from 'angular-archwizard';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import { FilterPipeModule } from 'ngx-filter-pipe';

/* Componentes */
import { RegistroComponent } from '../pages/registro/registro.component';
import { FormularioComponent } from './formulario/formulario.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { PanelAdminComponent } from './panel-admin/panel-admin.component';



@NgModule({
    declarations: [
        RegistroComponent,
        FormularioComponent,
        LoginAdminComponent,
        PanelAdminComponent
    ],

    exports: [
        RegistroComponent
    ],

    imports: [
        SharedModule,
        RouterModule,
        ArchwizardModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        FilterPipeModule
    ]

})

export class PagesModule { }
